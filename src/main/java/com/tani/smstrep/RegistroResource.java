/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tani.smstrep;

import com.google.gson.Gson;
import com.tani.smstrep.bd.SqliteDB;
import com.tani.smstrep.bd.StatusResponse;
import com.tani.smstrep.bd.SyBase;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * REST Web Service
 *
 * @author sergi
 */
@Path("registro")
public class RegistroResource {
 static final Logger logger = LogManager.getLogger(Logger.class.getName());
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RegistroResource
     */
    public RegistroResource() {
    }

    /**
     * Retrieves representation of an instance of com.personal.smstrep.RegistroResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces({"application/json"})
  //  @Path("/registrar")
    public String getJson(@QueryParam("linea") String linea,@QueryParam("local") String local,@QueryParam("mesa") String mesa,@QueryParam("orden") String orden) {
        //TODO return proper representation object
        this.logger.info("INGRESO:"+linea+"::"+local+"::"+mesa+"::"+orden);
        if(linea!=null && linea.length()>=13){
            linea = linea.substring(1);
        }
        //SqliteDB db = new SqliteDB();
        //db.insercion(linea, local, mesa, orden);
        SyBase db = new SyBase();
        StatusResponse res = db.CargarDatos(linea, local, mesa, orden);
        Gson g = new Gson();
        this.logger.info("SALIDA:"+res);
        return g.toJson(res);
    }
    
    
    /*
       @GET
       @Path("/test")
   
    
    public String test() throws  Exception{
        //TODO return proper representation object
        this.logger.info("INGRESO: test");
        //SqliteDB db = new SqliteDB();
        //db.insercion(linea, local, mesa, orden);
        SyBase db = new SyBase();
        String res = db.test("122");
        Gson g = new Gson();
        this.logger.info("SALIDA:"+res);
        return g.toJson(res);
    }
*/
    /**
     * PUT method for updating or creating an instance of RegistroResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJson(String content) {
    }
}
