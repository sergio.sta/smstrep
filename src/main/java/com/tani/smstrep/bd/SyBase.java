/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tani.smstrep.bd;



import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sergi
 */
public class SyBase {
     static final Logger log = LogManager.getLogger(Logger.class.getName());
  
  /*     */   private Statement stmt;
  //public Connection conectarBD(HashMap params)
          public Connection conectarBD()
    throws Exception
  {
   // Class.forName("com.sybase.jdbc3.jdbc.SybDriver");
    
     Class.forName("com.sybase.jdbc4.jdbc.SybDriver");
    Properties props = new Properties();

      props.put("user", "usrwebservice");

      props.put("password", "Hap01te2015");

      String url = "jdbc:sybase:Tds:" + "192.168.7.250" + ":" + "2620";
       //String url = "jdbc:sybase:Tds:" + params.get("IP DB") + ":" + params.get("Port DB");
  //  String url = "jdbc:sybase:Tds:" +"192.168.1.22" + ":" +"2638";
    
    this.log.info(" url [" + url + "]");
    Connection conn = DriverManager.getConnection(url, props);
    conn.setAutoCommit(true);
    return conn;
  }
  
  public void desconectarBD(Connection pConn)
    throws SQLException
  {
    pConn.close();
  }
  
    public String test(String idLog_) throws Exception
/*     */   {
/* 147 */     
/* 148 */     String query = null;
/*     */     String respuesta = "test";
/*     */     try
/*     */     {
               Connection  conexion = this.conectarBD();
/* 152 */       log.info("[" + idLog_ + "] Preparando la funcion [traer fecha hora]");
/*     */       
    //conexion.createStatement().execute("create variable a1 text");
      // conexion.createStatement().execute("call DBA.APP_VENTAS_FECHA(a1)");
       ResultSet rs = conexion.createStatement().executeQuery("select getdate();");
       
       
       if (rs.next()) {
           respuesta = rs.getString(1);
       }   else {
         log.info("[" + idLog_ + "] resultado: [error]");
      }
       rs.close();
       conexion.close();
   }

/*     */     catch (Exception ex) {
/* 178 */       log.error("[" + idLog_ + "] ACTIVACION - Error #5[" + ex.getMessage() + "]");
                ex.printStackTrace();
/* 179 */     
/*     */     }
/* 182 */     log.info("[" + idLog_ + "] respuesta [" + respuesta + "]");
             
/* 183 */     return respuesta;
/*     */   }
    
    
    
/*
    CREATE PROCEDURE APP_VENTAS_LISTAR_CLIENTES(  IN  mUsuario      CHAR(16)    ,
                                              OUT mListaCodigos TEXT        ,
                                              OUT mRetNumber    NUMERIC(7,0),
                                              OUT mRetMessage   CHAR(100))
    */
        

//Call APP_VENTAS_LISTAR_CLIENTES ( 'admin', mListaCodigos, mERROR, mMENSAJE);


    
    
    
    
    
    
    
  /*CREATE PROCEDURE APP_VENTAS_LOGIN(  IN  mUsuario      CHAR(16)   ,
                                    IN  mPWD          CHAR(20)   ,
                                    IN  mToken        CHAR(20)   ,
                                    OUT mRetNumber    NUMERIC(7,0),
                                    OUT mRetMessage   CHAR(100));*/
    
    
    
    
//Call APP_VENTAS_LOGIN ( 'admin', '654321', '12341', mERROR, mMENSAJE);    
    /*
    create variable retnum integer;
create variable retmes text;

CALL "DBA"."APP_VENTAS_LOGIN"('admin', 12313,213123, retnum , retmes);
select retnum,retmes;
    
    
    public StatusResponse APPVentasLogin(String user, String pass, String codigo) {
        ResultSet resultSet = null;
        String query = null;
        ResultSet rs = null ;
        StatusResponse res = new StatusResponse();
        CallableStatement scs = null;
        Connection conexion = null;;
        try{
         conexion = conectarBD();
        log.info(" Preparando la funcion [dba.APP_VENTAS_LOGIN]"+user);
          conexion.createStatement().execute(" create variable retnum integer;");
          conexion.createStatement().execute(" create variable retmes text;");
       conexion.createStatement().execute("CALL \"DBA\".\"APP_VENTAS_LOGIN\"('"+user+"', "+pass+","+codigo+", retnum , retmes);");
        rs = conexion.createStatement().executeQuery("select retnum, retmes;");
       
       
       if (rs.next()) {
            res.setEstado( rs.getInt(1));
        res.setMensaje( rs.getString(2));
        
       }   else {
         log.info("[ ] resultado: [error]");
      }
   
        
        rs.close();
//System.out.println(cs.getString(5)); 
      
        
        }catch(Exception e){
            e.printStackTrace();
            this.log.error(e.getMessage());
        
        }finally{
           
            try{
                 rs.close();
                
                 conexion.close();
            }catch(Exception ex){
                ;
            }
            
        }
        this.log.info(res);
        return res;
    }

   
  */

    
     
    public StatusResponse CargarDatos(String telefono, String local ,String mesa, String orden) {
    
        String query = null;
        ResultSet rs = null ;
        StatusResponse res = new StatusResponse();
        CallableStatement scs = null;
        Connection conexion = null;;
        try{
         conexion = conectarBD();
        log.info(" Preparando la funcion [dba.dba._WebS_Set_Voto]"+telefono);
        log.info("SELECT dba._WebS_Set_Voto("+local+", "+mesa+","+orden+", '"+telefono+"');");
       rs = conexion.createStatement().executeQuery("SELECT dba._WebS_Set_Voto("+local+", "+mesa+","+orden+", '"+telefono+"');");
     
       
       
        if (rs.next())
/*     */       {
/* 156 */         log.info("[ ] resultado: [" + rs.getString(1) + "]");
/*     */         
/* 158 */         StringTokenizer tokens = new StringTokenizer(rs.getString(1), "#");
/*     */         
/* 160 */         res.setMensaje(tokens.nextToken());
/* 161 */         
/* 162 */         res.setResultado(tokens.nextToken());
/*     */         
        }
   
        
        rs.close();
//System.out.println(cs.getString(5)); 
      
        
        }catch(Exception e){
            e.printStackTrace();
            this.log.error(e.getMessage());
        
        }finally{
           
            try{
                 rs.close();
                
                 conexion.close();
            }catch(Exception ex){
                ;
            }
            
        }
        this.log.info(res);
        return res;
    }

    
    
}
 
  

