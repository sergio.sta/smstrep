/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tani.smstrep.bd;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author sergi
 */
public class SqliteDB {
    
    /**
     * Connect to the test.db database
     * @return the Connection object
     */
    
    static final Logger logger = LogManager.getLogger(Logger.class.getName());
    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:/root/strep.bd";
        Connection conn = null;
       
        try {
             Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection(url);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return conn;
    }
    
    /**
     * select all rows in the warehouses table
     */
    public void insercion(String linea, String local , String mesa, String orden ){
        
        Integer ilocal = Integer.parseInt(local);
        
        Integer imesa = Integer.parseInt(mesa);
        
        
        Integer iorden = Integer.parseInt(orden);
        
        String sql = "insert into  strep_datos values ('"+linea+"',"+ilocal+","+imesa+","+iorden+");";
        
        try {
            Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             stmt.executeUpdate(sql);
            
           
        } catch (Exception e) {
          this.logger.error(e.getMessage());
          e.printStackTrace();
        }
    }
    
}
