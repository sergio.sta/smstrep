/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tani.smstrep.bd;

/**
 *
 * @author sergi
 */
public class StatusResponse {
    private String mensaje;
    private String resultado;

    @Override
    public String toString() {
        return "StatusResponse{" + "mensaje=" + mensaje + ", resultado=" + resultado + '}';
    }
    
    

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the resultado
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
}
